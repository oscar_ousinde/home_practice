'use strict';

//#################################DESTRUCTURING AN OBJECT#################################
/* const me = {
  name: 'Oscar',
  surname: 'Ousinde',
  age: 29,
  hobby: 'Guitar',
};

const { name, hobby } = me; 

console.log(name);*/

//#################################DESTRUCTURING AN ARRAY#################################
/* const myArray = [1, 2, 3, 4, 5];

const [, dos, , cuatro, cinco] = myArray;

console.log(cinco); */

//#################################DESTRUCTURING AN OBJECT and SPREAD#################################
/* const me = {
  name: 'Oscar',
  surname: 'Ousinde',
  age: 29,
  hobby: 'Guitar',
};

const { name, hobby, ...residual } = me;

console.log(residual);
console.log(me); */

//#################################DESTRUCTURING AN ARRAY and SPREAD#################################
//En la desestructuración con la coma despreciamos esos elementos del array. No van al SPREAD como en Objects

/* const myArray = [1, 2, 3, 4, 5];

const [, x, y, z, ...resto] = myArray;

console.log(`Números seleccionados: ${x}, ${y}, ${z}`);
console.log(`Números resto: ${resto}`); 
console.log(myArray); */
