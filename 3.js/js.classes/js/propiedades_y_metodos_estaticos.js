'use strict';

/**
 * las propiedades estáticas están asociadas a la clase pero no a los objetos que se instancia con ella.
 * Se definen con static name = ''; Se asocian estas propiedades a la clase donde se encuentran.
 */
class Book {
  static store = 'Mi librería';
  static description = 'Esta clase define un libre';

  static compareRelease() {
    console.log('Este es un método estático');
  }

  //Esta es un método estático con una funcionalidad para comprobar el stock de los libros
  //Como parámetro tiene books. Aquí podemos llamar a este método estático pasando como argumento la variable de la istancia de clase
  static available(books) {
    console.log(books.name);
    if (books.units > 0) {
      console.log(`Hay existencias! Quedan ${books.units}`);
    } else {
      console.log('Ya no quedan existencias...');
    }
  }

  constructor(name, release, units) {
    this.name = name;
    this.release = release;
    this.units = units;
  }

  sell() {
    if (this.units > 0) {
      this.units = this.units - 1;
      console.log(`Vendida una unidad de ${this.name} quedan ${this.units}`);
    }
  }
}

const book1 = new Book('Javascript Guide', new Date(2021, 1), 4);
const book2 = new Book('Learning HTML', new Date(2010, 5), 0);
const book3 = new Book('Learning CSS', new Date(2014, 10), 5);

//Esto nos lanzará un undefined! La propiedad estática store no está asociada a la instancia de la clase Book! Está asociada a la propia clase
console.log(book1.store);

//Para acceder a ella se hace con el nombre de la clase
//Son propiedades similares a metadatos en HTML. Todas las instancia de esa clase no tienen porqué llevar esta propiedad, está solo en la clase
//De la misma forma podemos definir métodos estáticos
console.log(Book.store);
console.log(Book.description);
Book.compareRelease();

//Podemos introducir lógica al crear métodos que nos permite crear herramientas de gestión
//Cada vez que se llama al método sell(), vendemos una unidad del libro que tenga más de 0 copias disponibles.
//Podemos utilizar aquí cualquier instancia creada book1 o book2
book1.sell();

//Llamamos al método estático available pasando como argumento la variable de la instancia de clase
//para comprobar la disponibilidad del mismo. Esta clase estática fomra parte de la clase Book pero
//no forma parte de las instancias de la clase. Podemos poner la función fuera de la clase si queremos e
//invocarla de fomra normal pero qued amejor encapsulada dentro de la clase y más ordenada como static.
Book.available(book1);
Book.available(book2);
Book.available(book3);
