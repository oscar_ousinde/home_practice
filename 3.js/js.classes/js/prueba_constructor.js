'use strict';

class User {
  constructor(name, surname) {
    this.name = name;
    this.surname = surname;
  }

  greet() {
    return `Hola me llamo ${this.name} ${this.surname}`;
  }
}

class Student extends User {
  constructor(name, surname, bootcamp) {
    super(name, surname);
    this.bootcamp = bootcamp;
  }
}

const oscar = new Student('Óscar', 'Ousinde', 'JSB13CO');
console.log(oscar);
console.log(oscar.greet());
