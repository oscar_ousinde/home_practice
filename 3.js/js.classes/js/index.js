'use strict';

/**
 * Una clase es un patrón para la creación de objetos.
 * En ella podemos estableces un constructor que recibe los parámetros que queramos enviar al crear la instancia.
 * Esta instancia no es más que un nuevo objeto con las propiedades y valores que establezca el constructor y los
 * parametros que le pasemos al generar la instancia.
 * Además de propiedades y valores las clases como los objetos pueden almacenar funcionalidad. Esta funciones son
 * conocidas como métodos. Una vez creada la instancia de la clase podemos llamar a estos métodos como lo haríamos
 * con cualquier objeto que los contiene.
 */
class Person {
  constructor(name, age, job) {
    this.name = name;
    this.age = age;
    this.job = job;
  }

  data() {
    return `Hello I am ${this.name} and I am ${this.age} years old`;
  }

  working() {
    return `I am working as ${this.job}`;
  }
}

//Creación de la instancia y llamada del nuevo objeto y de los métodos contenidos en ella.
const user1 = new Person('Oscar', 29, 'Developer');
console.log(user1);
console.log(user1.data());
console.log(user1.working());
console.log('=====================================================');

/**
 * Hay algo que se llaman propiedades calculadas en las clases.
 * Son propiedades que no están incluidas en el constructor. Algo así como propiedades virtuales.
 * Son creadas como métodos añadiendo la palabra reservada 'get'. Además para llamarlas hay
 * que hacerlo como una propiedad no ocmo un método. Utilizando la notación con punto y
 * sin paréntesis al final (eso solo se hace para las funciones y métodos). En resumen se escriben
 * como método pero se llaman como propiedad. Para esto hacemos uso de los getters y también podemos usar
 * los setters
 */

class User {
  constructor(name, surname) {
    this.name = name;
    this.surname = surname;
  }

  get fullName() {
    return this.name + ' ' + this.surname;
  }
}

const userOscar = new User('Óscar', 'Ousinde');
console.log(userOscar);

//llamada del método fullName como propiedad. ES UNA PROPIEDAD! no es un método. ES UN GETTER
console.log(userOscar.fullName);

//Las instancias de clases son objetos y podemos modificarlos como siempre.
//Después del cambio el getter fullName será Jose Ousinde. También podemos controlar esto -LEER MÁS ADELANTE-
userOscar.name = 'Jose';
console.log(userOscar.fullName);

//########################################SETTERS Y GETTERS######################################################
/**
 * El cambio de propiedades en una instancia de una clase es como la de un objeto. Sin embargo, podemos definir
 * SETTERS y GETTERS para que las propeidades no cambien tan facilmente! De esta forma podemos aplicar
 * condiciones a eses cambios de propiedad. Por ejemplo no permitir el registro de menores de 18 años. Un ejemplo:
 */

class AdultUsers {
  constructor(name, surname) {
    this.name = name;
    this.surname = surname;
  }

  /*   En el setter establecemos una propiedad 'age'. Al llamar a age como propiedad y cambiar el age
  este método va a ejecutarse. Puede existir un problema y es que al intentar modificar la propiedad age
  se ejecuta el método set age. El cual es una función que intenta modificar la propiedad age con this.age.
  Esto es un problema! puesto que entramos en un bucle, en un problema de recursión! La función al ejecutarse se
  llama a si misma (al setter) y este a su vez ejecuta la función y así hasta el infinito! No se puede guardar
  en la propiedad age. Se puede guardar en la propiedad _age. Esto es una propiedad PRIVADA y se utiliza con la 
  barra baja. Es un indicador para solo utilizarla intenamente.
  */

  set age(age) {
    if (age < 18) {
      throw new Error('Eres menor de 18! No puedes acceder');
    }
    this._age = age;
  }
  //Con el getter podemos devolver esa propiedad que antes guardamos en el setter y pasaría por sus condiciones!
  //Solo se puede acceder a la propiedad age desde el getter
  get age() {
    return this._age;
  }

  get fullName() {
    return `${this.name} ${this.surname} es mayor de edad`;
  }
}

//Definimos una estancia e intentamos definir un 'age' que va a pasar por el setter
const myUser = new AdultUsers('Oscar', 'Ousinde');

/**
 * En este momento modificamos la propiedad age que no está definida en el constructor de la clase.
 * Es decir, esta instancia myUser se compone de name y surname pero no de age. La propiedad age vamos a tratala como
 * una propiedad PRIVADA. Para ello podemos definirla normalmente, pero no se va a establecer como una propiedad de acceso
 * normal en el constructor. Va a utilizar getter y setter para ello. Al definir la propiedad age esta pasa primero por
 * el set age(age). Vemos como ese método recibe la popiedad age y se establece de forma interna con this._age = age; (CUIDADO CON LA RECURSIÓN
 * , ES EL MOTIVO DE LLAMARLA COMO _age). Esto se guarda así siempre y cuando pase la condición que nosotros establecimos! En este caso
 * solo usuarios mayores de 18 años. Si se cumple la propiedad _age ahora contiene la edad indicada al modificarla (29). Tras esto pasa al getter.
 * es llamado como get age para saber de que hablamos y lo que hace es un return de la propiedad _age donde almacenamos la edad en el setter.
 * Ahora podemos acceder al método fullName(llamado como propiedad de la instancia myUser), e imprimir el console.log del mismo. SETTERS Y GETTERS DEBEN IR POR LO TANTO EN
 * LA PARTE ALTA DE LA CLASE PARA EJECUTARSE PRIMERO-
 */
myUser.age = 29;
console.log(myUser.fullName);
console.log('=====================================================');

//########################################INSTANCIAS DE CLASE SIN GETTERS NI SETTERS#####################################################

/**
 * En lugar de usar getters y setters tabién podemos establecer las condiciones de ciertas propiedades dentro del constructor.
 * De esta forma nos podemos ahorrar usarlos. Tendremos entonces que escribir los parámetros adeecuados de la clase y también
 * pasar los argumentos necesarios cuando creamos la instancia de la clase
 */

//Clase Cars
class Cars {
  constructor(brand, model, year) {
    //Condicional para año del coche a registrar
    if (year < 2000) {
      throw new Error(
        'No se permite el registro de coches anteriores al año 2000'
      );
    }
    this.brand = brand;
    this.model = model;
    this.year = year;
  }

  //Método de clase con un parámetro
  myCar(newOrOld) {
    return `My ${newOrOld} car is a ${this.brand} ${this.model} of the ${this.year}`;
  }

  //Getters para usar un método como propeidad. Los getters no pueden tener parámetros. Se usan como propiedades de objetos
  get myCar2() {
    return `My car is a ${this.brand} ${this.model} of the ${this.year}`;
  }
}

//Instancia de Cars
const newCar = new Cars('hyundai', 'ioniq', 2019);

//Llamada del método myCar
console.log(newCar.myCar('new'));

//Llamada del método como propiedad al ser un getter
console.log(newCar.myCar2);

//Podemos establecer una codición dentro del constructor de no aceptar coches de antes del año 2000 y devolver un error
//al intentar establecer una propiedad de una instancia que incumpla la condición
//Se lanza el error y no se permite acceder al getter myCar2! Se detiene la ejecución
const oldCar = new Cars('Ford', 'Mustang', 1972);
console.log(oldCar.myCar2);
