'use strict';

class Person {
  constructor(name) {
    this.name = name;
    this.sleeping = false;
  }

  sleep() {
    this.sleeping = true;
    console.log(`${this.name} empieza a dormir`);
  }

  wakeup() {
    this.sleeping = false;
    console.log(`${this.name} se despierta`);
  }
}

//Queremos que Student herede de Person. Esta clase no tiene constructor pero lo hereda de Person.
//Podemos crear un constructor de todas formas pero hay que llamar al constructor padre antes de utilizar this. con super
// + la propiedad que espera al constructor padre
class Student extends Person {
  constructor(name, bootcamp) {
    super(name);
    this.bootcamp = bootcamp;
  }
  study() {
    //Hereda de la clase Person la propeidad name!
    console.log(
      `${this.name} empieza a estudiar para el bootcamp ${this.bootcamp}`
    );
  }

  //Este método sobreescribe a la de herencia por tener el mismo nombre. Por lo tanto este tiene prioridad!
  wakeup() {
    console.log(`${this.name} no se quiere levantar`);
  }
}

const tania = new Student('Tania', 'JSB13CO');

//Esto arroja undefined empieza a dormir. La nueva instancia creada como tania de la clase Student hereda de Person.
//Pero Person tiene un parámetro, por lo tanto necesitamos pasar un argumento al crear la isntancia. El método sleep lo toma de la clase Person de la que hereda
// tania.sleep(); --> esto arroja undefined
tania.sleep(); //método de la clase Person
tania.wakeup(); //método de la clase Student (sobreescrito). Se puede llamar al método Padre con super.wakeup()
tania.study(); //método de la clase Student

console.log(tania);
//Si creamos una instancia de Person y llamamos con ella al método study() esto nos dará un error dado que
//ese método no existe en la clase Person. Está en la Student.
