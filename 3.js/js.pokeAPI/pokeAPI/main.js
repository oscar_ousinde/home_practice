'use strict';

//Selección de la lista donde vamos a listar los pokemons
const pokeList = document.querySelector('ul.pokemons');

//Solicitud de los pokemons a API
const getPokemons = async (url = 'https://pokeapi.co/api/v2/pokemon/') => {
  try {
    //Petición al server
    const response = await fetch(url);
    console.log(response);

    //Obtengo la info del body
    const body = await response.json();
    console.log(body);

    //Vacio el ul
    pokeList.innerHTML = '';

    //Recorro el array results
    for (const pokemons of body.results) {
      //Peticion a la url de cada elemento para obtener los sprites
      const sprites = async (urlSprites = pokemons.url) => {
        try {
          //Peticion al server para sprites
          const responseSprites = await fetch(urlSprites);
          console.log(urlSprites);
        } catch (errSprites) {
          console.error(errSprites);
        }
      };
      //Creo los li
      const li = document.createElement('li');

      //Agrego contenido al li creado
      li.innerHTML = `
        <img src="""" alt="${pokemons.name}"
        <h3>${pokemons.name}</h3>
        `;

      //Pusheo dentro del ul
      pokeList.append(li);
    }
  } catch (err) {
    console.error(err);
  }
};

//Llamada a la función
getPokemons();
