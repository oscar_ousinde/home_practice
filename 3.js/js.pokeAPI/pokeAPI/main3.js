'use strict';

//Selecciono el formulario pokeForm
const pokeForm = document.forms.pokeForm;

//Selecciono el ul
const pokeList = document.querySelector('ul.pokemons');

//EVENTO SUBMIT AL FORM
pokeForm.addEventListener('submit', (e) => {
  //Cancelar el comportamiento por defecto del formulario
  e.preventDefault();

  //Variable del valor del input del form
  const pokeSearch = pokeForm.elements.pokeSearch.value;

  //PETICIÓN API
  const getPokemon = async (pokeSearch) => {
    const response = await fetch(
      `https://pokeapi.co/api/v2/pokemon/${pokeSearch}/`
    );
    const data = await response.json();

    //Destructuring data
    const { name, sprites } = data;

    //PARTE DEL PUSHING A HTML
    //Limpiamos la ul.pokemons --> pokeList
    pokeList.innerHTML = '';

    //Creo el li
    const li = document.createElement('li');

    //Agrego contenido al li
    li.innerHTML = `
        <img src=${sprites.front_default} alt=${name}/>
        <h3>${name}</h3>
    `;

    //Append a pokeList del li
    pokeList.append(li);

    //Reinicio la barra search pokeSearch
    pokeSearch = '';
  };

  //Llamada a la función getPokemon (llamada al server)
  getPokemon(pokeSearch);
});
