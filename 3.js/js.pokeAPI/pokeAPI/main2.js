'use strict';

//Selección de lista pokemons y el form
const pokeList = document.querySelector('ul.pokemons');
const form = document.pokeForm;

//Solicitud a la API
const getPokemon = async (numberOrName) => {
  const response = await fetch(
    `https://pokeapi.co/api/v2/pokemon/${numberOrName}`
  );
  console.log(response);
  const data = await response.json();
  console.log(data);

  renderPokemon(data);
};

const renderPokemon = (data) => {
  const { name, sprites } = data;

  pokeList.innerHTML = '';

  const li = document.createElement('li');

  li.innerHTML = `
      <img src="${sprites.front_default}" alt="${name}"/>
      <h3>${name}</h3>
    `;

  pokeList.append(li);
};

//Llamada a la función getPokemon
getPokemon(25);
