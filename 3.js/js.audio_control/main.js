'use strict';

const track = document.querySelector('audio');
const musicBtnPlay = document.querySelector('.musicBtnPlay');

musicBtnPlay.addEventListener('click', () => {
  //   track.paused ? track.play() : track.pause();
  if (track.paused) {
    track.play();
    musicBtnPlay.src = 'icons/soundOn.svg';
  } else {
    track.pause();
    musicBtnPlay.src = 'icons/soundOff.svg';
  }
});
