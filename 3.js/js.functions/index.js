'use strict';

//Función flecha con solo una declaración. Puede ir sin {}.
//Primer callback

/* const suma = (a, callback) => {
  console.log(a - 1);
  callback();
};

suma(10, () => {
  console.log('Esto es una resta');
});
 */

//Más callback

/* function cuenta(a, b) {
  console.log(a + 2);
  b(a, 2);
}

cuenta(10, function (c, d) {
  console.log(c * d);
  console.log('Esto es un callback');
}); */

//MÁS CALLBACK
/**
 * Creamos la función doHomework con dos parámetros.
 * Uno de ellos forma parte de la ejecución del bloque de código de la función (subject).
 * El otro es una función de callback que invocamos dentro de la función principal (que es de High Order porque acepta funciones como parámetros).
 * Al invocar la función doHomework pasamos el primer argumento y el segundo como una función de flecha en este caso.
 * Esta función anónima de flecha puede tener también parámetros y sus argumentos serán pasados desde la invocación de la
 * función doHomework que es desde donde es llamada.
 */

/* function doHomework(subject, callback) {
  alert(`Starting my ${subject} homework`);
  callback('History');
}

doHomework('Math', (subjectA) => {
  alert(`Finished my homework of ${subjectA}`);
}); */

//PRÁCTICA CALLBACK

const user = (nombre, apellido) => {
  console.log(`Hola me llamo ${nombre}`);
  apellido('Ousinde');
};
user('Óscar', (argumentoDeLaFunciónCallbackApellido) =>
  console.log(`Y mi apellido es ${argumentoDeLaFunciónCallbackApellido}`)
);
