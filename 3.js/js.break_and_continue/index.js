'use strict';

//BREAK

/* let x = -6;

let y = 5;

let numero = 3;

for (x; x <= y; x++) {
  console.log(x);
  if (x === numero) {
    break;
  }
} */

//CONTINUE

/* let i = 0;
let n = 0;

while (i < 5) {
  i++;
  if (i === 3) {
    continue;
  }
  n += i;
  console.log(n);
} */

const vowels = ['a', 'e', 'i', 'o', 'u'];

for (let vowel in vowels) {
  console.log(vowel);
}

for (let vowel in vowels) {
  console.log(vowels[vowel]);
}

for (let vowel of vowels) {
  console.log(vowel);
}

for (let i = 0; i < vowels.length; i++) {
  console.log(vowels[i]);
}
