'use strict';

//Array.map()
//Returns a new array with the results of calling a provided function on every element in this array.
const listMap = [1, 2, 3, 4];
listMap.map((el) => el * 2); // [2,4,6,8]

//Array.filter()
//Returns a new array with all elements that pass the test implemented by the provided function.
const listFilter = [1, 2, 3, 4];
listFilter.filter((el) => el % 2 === 0); // [2, 4]

//Array.reduce()
//Reduce the array to a single value. The value returned by the function is stored in an accumulator (result/total).
const listReduce = [1, 2, 3, 4, 5];
listReduce.reduce((total, item) => total + item, 0); // 15

//Array.reduceRight()
//Executes a reducer function (that you provide) on each element of the array resulting in a single output value(from right to left).
const listReduceRight = [1, 2, 3, 4, 5];
listReduceRight.reduceRight((total, item) => total + item, 0); // 15

//Array.fill()
//Fill the elements in an array with a static value.
const listFill = [1, 2, 3, 4, 5];
listFill.fill(0); // [0, 0, 0, 0, 0]

//Array.find()
//Returns the value of the first element in the array that satisfies the provided testing function. Otherwise undefined is returned.
const listFind = [1, 2, 3, 4, 5];
listFind.find((el) => el === 3); // 3
listFind.find((el) => el === 6); // undefined

//Array.indexOf()
//Returns the first index at which a given element can be found in the array, or -1 if it is not present.
const listIndexOf = [1, 2, 3, 4, 5];
listIndexOf.indexOf(3); // 2
listIndexOf.indexOf(6); // -1

//Array.lastIndexOf()
//Returns the last index at which a given element can be found in the array, or -1 if it is not present. The array is searched backwards, starting at fromIndex.
const listLastIndexOf = [1, 2, 3, 4, 5];
listLastIndexOf.lastIndexOf(3); // 2
listLastIndexOf.lastIndexOf(3, 1); // -1

//Array.findIndex()
//Returns the index of the first element in the array that satisfies the provided testing function. Otherwise -1 is returned.
const listFindIndex = [5, 12, 8, 130, 44];
listFindIndex.findIndex((element) => element > 13); // 3

//Array.includes()
//Returns true if the given element is present in the array.
const listIncludes = [1, 2, 3, 4, 5];
listIncludes.includes(3); // true
listIncludes.includes(6); // false

//Array.pop()
//Removes the last element from an array and returns that element.
const listPop = [1, 2, 3, 4, 5];
listPop.pop(); // 5
listPop; // [1, 2, 3, 4]

//Array.push()
//Appends new elements to the end of an array, and returns the new length.
const listPush = [1, 2, 3, 4, 5];
listPush.push(6); // 6
listPush; // [1, 2, 3, 4, 5, 6]

//Array.shift()
//Removes the first element from an array and returns that element.
const listShift = [1, 2, 3, 4, 5];
listShift.shift(); // 1
listShift; // [2, 3, 4, 5]

//Array.unshift()
//Adds new elements to the beginning of an array, and returns the new length.
const listUnshift = [1, 2, 3, 4, 5];
listUnshift.unshift(0); // 6
listUnshift; // [0, 1, 2, 3, 4, 5]

//Array.splice()
//Changes the contents of an array by removing or replacing existing elements and/or adding new elements in place.
const listSplice = [1, 2, 3, 4, 5];
listSplice.splice(1, 2); // [2, 3]
listSplice; // [1, 4, 5]

const months = ['Jan', 'March', 'April', 'June'];
months.splice(1, 0, 'Feb');
// inserts at index 1
console.log(months);
// expected output: Array ["Jan", "Feb", "March", "April", "June"]

months.splice(4, 1, 'May');
// replaces 1 element at index 4
console.log(months);
// expected output: Array ["Jan", "Feb", "March", "April", "May"]

//Array.slice()
//Returns a shallow copy of a portion of an array into a new array object selected from begin to end (end not included). The original array will not be modified.
const list = [1, 2, 3, 4, 5];
list.slice(1, 3); // [2, 3]
list; // [1, 2, 3, 4, 5]

//Array.join()
//Joins all elements of an array into a string.
const listJoin = [1, 2, 3, 4, 5];
listJoin.join(', '); // "1, 2, 3, 4, 5"

//array.reverse()
//Reverses the order of the elements in an array.
const listReverse = [1, 2, 3, 4, 5];
listReverse.reverse(); // [5, 4, 3, 2, 1]
listReverse; // [5, 4, 3, 2, 1]

//Array.sort()
//Sorts the elements of an array in place and returns the array. The default sort order is according to string Unicode code points.
const arraySort = [4, 1, 3, 2, 10];
arraySort.sort(); // 😧 [1, 10, 2, 3, 4]
arraySort.sort((a, b) => a - b); // 😀 [1, 2, 3, 4, 10]

const arraySort2 = ['D', 'B', 'A', 'C'];
arraySort2.sort(); // 😀 ['A', 'B', 'C', 'D']

//Array.some()
//Returns true if at least one element in the array passes the test implemented by the provided function.
const listSome = [1, 2, 3, 4, 5];
listSome.some((el) => el === 3); // true
listSome.some((el) => el === 6); // false

//Array.every()
//Returns true if all elements in the array pass the test implemented by the provided function.
const listEvery = [1, 2, 3, 4, 5];
listEvery.every((el) => el === 3); // false

const listEvery2 = [2, 4, 6, 8, 10];
listEvery2.every((el) => el % 2 === 0); // true

//Array.from()
//Creates a new array from an array-like or iterable object.
const listArrayFrom = '😀😫😀😫🤪';
Array.from(list); // [😀, 😫, 😀, 😫, 🤪]

const listArrayFrom2 = new Set(['😀', '😫', '😀', '😫', '🤪']);
Array.from(set); // [😀, 😫, 🤪]

//Array.of()
//Creates a new array with a variable number of arguments, regardless of number or type of the arguments.
const listArrayOf = Array.of(1, 2, 3, 4, 5);
listArrayOf; // [1, 2, 3, 4, 5]

//Array.isArray()
//Returns true if the given value is an array.
Array.isArray([1, 2, 3, 4, 5]); // true
Array.isArray(5); // false

//Array.at()
//Returns a value at the specified index.
const listArrayAt = [1, 2, 3, 4, 5];
listArrayAt.at(1); // 2
listArrayAt.at(-1); // 5
listArrayAt.at(-2); // 4

//Array.copyWithin()
//Copies array elements within the array. Returns the modified array.
const listCopyWithin = [1, 2, 3, 4, 5];
listCopyWithin.copyWithin(0, 3, 4); // [4, 2, 3, 4, 5]

/**
 * first argument is the target at which to start copying elements from.
 * second argument is the index at which to start copying elements from.
 * third argument is the index at which to stop copying elements from.
 */

//Array.flat()
//Returns a new array with all sub-array elements concatenated into it recursively up to the specified depth.
const listFlat = [1, 2, [3, 4, [5, 6]]];
listFlat.flat(Infinity); // [1, 2, 3, 4, 5, 6]

//Array.flatMap()
//Returns a new array formed by applying a given callback function to each element of the array,
const listFlatMap = [1, 2, 3];
listFlatMap.flatMap((el) => [el, el * el]); // [1, 1, 2, 4, 3, 9]
