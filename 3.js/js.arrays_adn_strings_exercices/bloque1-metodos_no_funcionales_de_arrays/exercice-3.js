'use strict';
// Dado el siguiente array:

// ['m','e',' ','e','n','c','a','n','t','a',' ','j','a','v','a','s','c','r','i','p','t']
// Guarda dos números que pidas al usuario por prompt (entre 0 y la longitud del array).

// Utiliza esos números para devolver un trozo del array.

// Guardarlo en un array nuevo y muestralo.

// Convierte el trozo del array que has sacado a string y muestralo por consola.

const arrayEjemplo = [
  'm',
  'e',
  ' ',
  'e',
  'n',
  'c',
  'a',
  'n',
  't',
  'a',
  ' ',
  'j',
  'a',
  'v',
  'a',
  's',
  'c',
  'r',
  'i',
  'p',
  't',
];

const num1 = prompt(`Introduce un número entre 0 y ${arrayEjemplo.length}`);

const num2 = prompt(
  `Introduce un segundo número entre 0 y ${arrayEjemplo.length} mayor a ${num1}`
);

const newArray = [];
const trozoArray = newArray.push(arrayEjemplo.slice(num1, num2).join(' '));

const newString = newArray[0];

console.log(newString);
