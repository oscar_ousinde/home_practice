'use strict';
// Crea un array con la clasificación de una carrera similar a este:

// ['Antonio', 'Maria', 'Juan', 'Carmen', 'Julia']
// Vamos a suponer que es el orden de la clasificación de un evento

// Durante el transcurso del mismo se modifican las siguientes posiciones

// Julia es eliminada del concurso.
// Detras de Maria y antes de Juan se clasifica un nuevo concursante 'Ramon'
// Descalifican a Antonio que iba líder hasta el momento
// Se clasifica un nuevo concursante que pasa a ser el primero 'Antonia'
// Al finalizar el concurso declaran que el orden va a ser el inverso, por lo que se da la vuelta a toooda la clasificación
// Imprime por pantalla el resultado de las modificaciones en el array de clasificación.

const clasification = ['Antonio', 'Maria', 'Juan', 'Carmen', 'Julia'];

// Julia es eliminada del concurso.
clasification.pop();

// Detras de Maria y antes de Juan se clasifica un nuevo concursante 'Ramon'
clasification.splice(2, 0, 'Ramon');

// Descalifican a Antonio que iba líder hasta el momento
clasification.shift();

// Se clasifica un nuevo concursante que pasa a ser el primero 'Antonia'
clasification.unshift('Antonia');

// Al finalizar el concurso declaran que el orden va a ser el inverso, por lo que se da la vuelta a toooda la clasificación
clasification.reverse();

//Array modificado
console.log(clasification);
