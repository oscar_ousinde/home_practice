'use strict';
// Crea un programa que pida por prompt un número y lo añada a un array vacío varias veces hasta que se indique la palabra 'STOP'.

// Una vez se haya indicado que pare de pedir números, muestra el array entero por consola (todos sus elementos)

const myArray = [];
let number = 0;

while (number !== 'STOP') {
  number = prompt(
    'Introduce un número o la palabra STOP para imprimir el array'
  );
  if (number === 'STOP') break;
  myArray.push(number);
}

console.log(myArray);
