'use strict';
// Dado el array [3, 10, 'Juan', 'Maria', 'Isabel', 'Antonio'] nos piden que elimines los 2 primeros elementos y guardes el resultado en un nuevo array.

// A este nuevo array solamente con los nombres, añade al inicio un nombre que indique el usuario por prompt.

const arrayDado = [3, 10, 'Juan', 'Maria', 'Isabel', 'Antonio'];

const newArray = arrayDado.slice(2, arrayDado.length);

newArray.unshift(prompt('Añade un nuevo nombre al array!'));

console.log(newArray);
