'use strict';
// Hasta la fecha el mundial de F1 tiene las siguientes puntuaciones:

const mundialF1 = [
  {
    nombre: 'Alonso',
    nacionalidad: 'es',
    puntuaciones: [2, 0, 0, 0, 8],
  },
  {
    nombre: 'Leclerc',
    nacionalidad: 'mco',
    puntuaciones: [26, 19, 26, 8, 18],
  },
  {
    nombre: 'Verstappen',
    nacionalidad: 'nl',
    puntuaciones: [0, 25, 0, 26, 26],
  },
  {
    nombre: 'Perez',
    nacionalidad: 'mx',
    puntuaciones: [0, 12, 18, 18, 12],
  },
  {
    nombre: 'Sainz',
    nacionalidad: 'es',
    puntuaciones: [18, 15, 0, 0, 15],
  },
  {
    nombre: 'Russel',
    nacionalidad: 'en',
    puntuaciones: [12, 10, 15, 12, 10],
  },
];

// Crea un nuevo array con el nombre de cada piloto y el total de puntos acumulados hasta el momento.

// Ordena el array y muestra por pantalla la clasicación del mundial de pilotos.
