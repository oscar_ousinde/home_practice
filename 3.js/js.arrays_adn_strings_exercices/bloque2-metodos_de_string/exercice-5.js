'use strict';
// Convierte la siguiente cadena de texto en un array en el cual cada elemento de este sea cada una de las palabras del texto:

// "Hola Mundo 😃"

// En qué posición se encuentra el carácter '😃'?

// Reemplaza el string 'Hola' por 'Adios' y '😃' por '😔'.

const words = 'Hola mundo 😃';

const array = [];
const arrayWords = words.split(' ');
for (const word of arrayWords) {
  array.push(word);
}
console.log(array);

const positionSmiley = array.indexOf('😃');
console.log(`El 😃 se encuentra en la posición ${positionSmiley}`);

const newArray = [];
for (let words of array) {
  if (words === 'Hola') {
    words = 'Adiós';
    newArray.push(words);
  } else if (words === '😃') {
    words = '😀';
    newArray.push(words);
  } else {
    newArray.push(words);
  }
}

console.log(newArray);
