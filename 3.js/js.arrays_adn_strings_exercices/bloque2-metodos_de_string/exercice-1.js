'use strict';
// Dada la siguiente cadena de texto.

// "Vendo Opel Corsa"

// Corta la cadena de texto por las posiciones que indique el usuario por prompt y muestralo por consola.

const opelCorsa = 'Vendo Opel Corsa';

const num1 = prompt(`Indica un número entre 0 y ${opelCorsa.length}`);
const num2 = prompt(`Indica un número entre ${num1} y ${opelCorsa.length}`);

const newString = opelCorsa.slice(num1, num2);

console.log(newString);
