'use strict';
// A partir de la siguiente cadena de texto "Hola Mundo!":

// Reemplaza todas las letras 'a' por 'i' y muestralo.

// Muestra solamente la palabra "Mundo" por consola.

const holaMundo = 'Hola mundo!';

const changed = holaMundo.replaceAll('a', 'i');

const soloMundo = changed.slice(0, 5);

console.log(changed);
console.log(soloMundo);
