'use strict';
// Comprueba si la letra que indica el usuario se encuentra en alguna de las siguientes palabras:

// "sandia", "platano", "melon"

// La aplicación no debe distinguir entre mayúsculas y minúsculas para encontrar la letra.

const userLetter = prompt('Introduce una letra');

const userLetterMin = userLetter.toLocaleLowerCase();
const userLetterMay = userLetter.toLocaleUpperCase();

const sandia = 'sandia';
const platano = 'platano';
const melon = 'melon';

for (const letter of sandia) {
  if (letter === userLetterMin) {
    console.log(`La letra ${userLetter} está incluída en la palabra ${sandia}`);
  } else if (letter === userLetterMay) {
    console.log(`La letra ${userLetter} está incluída en la palabra ${sandia}`);
  }
}

for (const letter of platano) {
  if (letter === userLetterMin) {
    console.log(
      `La letra ${userLetter} está incluída en la palabra ${platano}`
    );
  } else if (letter === userLetterMay) {
    console.log(
      `La letra ${userLetter} está incluída en la palabra ${platano}`
    );
  }
}

for (const letter of melon) {
  if (letter === userLetterMin) {
    console.log(`La letra ${userLetter} está incluída en la palabra ${melon}`);
  } else if (letter === userLetterMay) {
    console.log(`La letra ${userLetter} está incluída en la palabra ${melon}`);
  }
}
