'use strict';
// Pide al usuario una frase que indique por prompt y:

// Muestra la frase con todas las letras en mayúscula.

// Muestra la frase con todas las letras en minúscula.

// Muestra la longitud total de la cadena de texto.

const userText = prompt('Escribe aquí tu frase');

const userTextMayus = userText.toLocaleUpperCase();
console.log(userTextMayus);

const userTextMinus = userText.toLocaleLowerCase();
console.log(userTextMinus);

const userTextLong = userText.length;
console.log(
  `El texto introducido tiene una longitud de ${userTextLong} caracteres`
);
