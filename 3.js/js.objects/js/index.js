'use strict';

//OBJETO LITERAL
const person = {
  name: 'Óscar',
  surname: 'Ousinde',
  age: 29,
  hobby: 'Guitar',
};

//OBJETO NEW
//seguido de new se realiza una función de invocación Object(). Esto es un CONSTRUCTOR.
let persona = new Object();

persona = {
  name: 'Óscar',
  surname: 'Ousinde',
  age: 29,
  hobby: 'Guitar',
};

//OBJETO Object.create()
let personinha = Object.create({ name: 'Óscar', surname: 'Ousinde' });

//CREAR, ELIMINAR Y ACCEDER A PROPIEDADES Y VALORES
//crea la propiedad y valor
person.pet = 'Pinha';
console.log(person);

//elimina propiedad y valor
delete persona.hobby;
console.log(persona);

//acceder a propiedades
console.log(person.name);
console.log(person['name']);

//PALABRA CLAVE This
/**
 * para acceder a propiedades y valores del propio objeto donde se encuentre el método.
 * Un método es un valor de una propiedad de un objeto dado como función.
 */

const myObject = {
  nombre: 'Oscar Ousinde',
  age: 29,
  sayHello: (user) => console.log(`Hi ${user}!`),
  sayBye: function () {
    console.log('Hi ' + this.nombre + ' and I am ' + this.age);
  },
};

myObject.sayHello('Óscar'); //uso del método sayHello. Utilizar función de invocación y pasar argumento/s.

myObject.sayBye(); //uso de this en el método para acceder a propiedades del objeto. NO FUNCIONA CON ARROW FUNCTIONS

//RECORRER OBJETOS
const coches = {
  tipo: 'deportivo',
  marca: 'Porsche',
  color: 'negro',
  fabricacion: 2010,
};
//Object.keys(obj)
//Devuelve un array con las propiedades del objeto
console.log(Object.keys(coches));

//Object.values(obj)
//Devuelve un array con los valores
console.log(Object.values(coches));

//Object.entries(obj)
//Devuelve array de arrays con los pares propiedad/valor
console.log(Object.entries(coches));

//recorrer objetos

/**
 * Con el bucle for of recorremos el objeto.
 * En este caso con Object.keys y despues imprimimos sus valores
 * accediendo con la variable prop que recorre cada propiedad con el acceso
 * coches[prop].
 * Es decir. La constante prop va tomando los valores de las propiedades
 * e imprimimos cada una + el valor accediendo a cada valor de cada propiedad prop.
 */
for (const prop of Object.keys(coches)) {
  console.log(`${prop}: ${coches[prop]}`);
}

//podemos desestructurar en el loop
for (const [prop, value] of Object.entries(coches)) {
  console.log(`${prop}: ${value}`);
}
