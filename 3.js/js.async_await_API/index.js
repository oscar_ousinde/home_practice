'use strict';

//Función asíncrona
const pokemon = async () => {
  try {
    //Petición API a la lista pokemon
    const response = await fetch('https://pokeapi.co/api/v2/pokemon');

    //Recojo la promise Response y se pasa s json.
    const pokeData = await response.json();

    //Un log de pokeData.results. Estoy entrando ya en el objeto devuelto en la lista de pokemon
    console.log(pokeData.results);

    //Creo array, recorro el array pokeData.results entrando en cada elemento (que son objetos) y tomo
    //El name de cada pokemon. Se pushea esto en el array creado
    const namesArray = [];
    for (const names of pokeData.results) {
      namesArray.push(names.name);
    }

    //Un log del array creado con los 20 primeros pokemon de la generación 1.
    console.log(namesArray);

    //Un cath error
  } catch (err) {
    console.log(err);
  }
};

pokemon();
