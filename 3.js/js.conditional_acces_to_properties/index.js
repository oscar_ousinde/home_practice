'use strict';

const myObject = {
  a: 'Oscar',
  b: null,
  c: 'non-null',
};

console.log(myObject.a);
//No hay problema en acceder a la propiedad a del objeto myObject.

console.log(myObject.a.b);
//Lanza un undefined por ser valor null la propiedad b.

/* console.log(myObject.a.b.c); */
//TypeError!!. La propiedad b tiene un valor de null. Al leer la siguiente propiedad nos lanza un TypeError. Dejamos esto comentado para poder operar con el resto de expresiones.

console.log(myObject.a.b?.c);
// Con el operador condicional de acceso a propiedades ?, estamos utilizando el optional chaining. Toda la expresión será calificada como undfined.

console.log(myObject.d);
//Propiedad d no definida. Nos arroja un undefined.

/* console.log(myObject.d.a); */
//El problema está al saltar de un acceso a propiedad calificado como undefined o null (d) a otro acceso de propiedad. Es cuando se produce el TypeError. Necesario optional chaining.

console.log(myObject.d?.a);
//De esta forma la expresión anterior nos arroja un undefined! Usamos el optional chaining.
