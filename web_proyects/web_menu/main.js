'use strict';

const nav = document.querySelector('.nav');
const menuList = document.querySelector('.menu-list');

const buttonMenu = document.querySelector('.menu');

buttonMenu.addEventListener('click', () => {
  nav.classList.toggle('nav-open');
  menuList.classList.toggle('open');
});
