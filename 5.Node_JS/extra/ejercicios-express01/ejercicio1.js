// ### Ejercicio 1
// * Servidor web que escucha cualquier request en el puerto 3000, y devuelve siempre status `200 OK` con el body:

const dotenv = require('dotenv').config();
const { PORT } = process.env;

const express = require('express');
const app = express();

// app.use((req, res) => {
//   res.status(200).send({
//     curso: 'backend',
//   });
// });

// ### Ejercicio 2

// - Cuando se llama a la ruta `/curso`, devuelve status `200 OK` con el body:

// ```javascript
// {
//   curso: 'backend';
// }
// ```
// - Cuando se llama a cualquier ruta distinta devuelve status `200 OK` con el body:

// ```javascript
// {
//   message: 'Hello world!';
// }
// ```

app.get('/curso', (req, res, next) => {
  res.send({
    curso: 'backends /curso',
  });
  next();
});

// app.use((req, res) => {
//   res.status(200).send({
//     message: 'Hello World!',
//   });
// });

// ### Ejercicio 3

// - Servidor web que escucha en el puerto 3000.
// - Cuando se llama al _endpoint_ `/curso`, devuelve status `200 OK` con el body:

// ```javascript
// {
//   curso: 'backend';
// }
// ```

// - Cuando se llama al _endpoint_ `/message`, devuelve status `200 OK` con el body:

// ```javascript
// {
//   message: 'Hello world!';
// }
// ```

app.get('/message', (req, res, next) => {
  res.status(200).send({
    message: 'Hello world!! /message',
  });
  next();
});

// - Cuando se llama a cualquier otro _endpoint_, devuelve status `404 NOT FOUND` con el body:

// ```javascript
// {
//   message: 'No lo encuentro';
// }
// ```

// app.use((req, res) => {
//   res.status(404).send({
//     message: 'Not found! 404',
//   });
// });

// ### Ejercicio 4

// - Servidor que se comporta igual que el del ejercicio 3, pero además imprime por pantalla el _método_ y la _URL_ de cada request.

// app.use((req, res) => {
//   console.log(req.method);
//   console.log(req.url);
// });

// ### Ejercicio 5

// - Servidor web que escucha cualquier request.
// - Cuando la request es un `POST` a `/data`, se devuelve el _JSON_ recibido.

app.post('/data', (req, res) => {
  res.send('json');
});

// - Cuando es otra request cualquiera, se responde `404 NOT FOUND` sin body.
app.use((req, res) => {
  res.status(404).send({});
});

// ### Ejercicio 6

// - Servidor web que gestiona objetos del tipo:

// ```javascript
// {
//   email: 'pepito@gmail.com',
//   message: 'Hola soy Pepito',
// }
// ```

// - Internamente usará un fichero `database.json` donde se almacenarán datos. Este archivo se sitúa en el directorio `/database` dentro de la raiz del servidor.
// - Cuando la request es un `GET` a `/api/messages`, se sirven al cliente con todos los datos que hay en el _JSON_ de almacenamiento.
// - Cuando la request es un `POST` a `/api/messages`, se incluye el objeto recibido en el _JSON_ de almacenamiento, y se responde al cliente con todos los datos que hay en el _JSON_ de almacenamiento.
// - Cuando es otra request cualquiera, se responde `404 NOT FOUND` sin body.

app.listen(PORT, () => {
  console.log(`Server running on PORT: http://localhost:${PORT}`);
});
